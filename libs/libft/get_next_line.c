/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 18:30:22 by elyahove          #+#    #+#             */
/*   Updated: 2016/12/17 20:32:12 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

t_fd	*find_fd(int fd, t_fd **fd_struct)
{
	t_fd	*new;

	if (!fd_struct || !*fd_struct)
	{
		if (!fd_struct)
			fd_struct = (t_fd**)malloc(sizeof(t_fd*));
		new = (t_fd*)malloc(sizeof(t_fd));
		new->next = NULL;
		new->fd = fd;
		new->l_st = ft_strdup("");
		*fd_struct = new;
		return (new);
	}
	new = *fd_struct;
	while (new->next)
	{
		if (new->fd == fd)
			return (new);
		new = new->next;
	}
	if (new->fd == fd)
		return (new);
	new->next = find_fd(fd, NULL);
	new = new->next;
	return (new);
}

char	*compose(t_list *string)
{
	int		el;
	t_list	*temp[2];
	char	*res;

	temp[0] = string;
	if (temp[0] == NULL)
		return (ft_strdup(""));
	el = 1;
	while (temp[0]->next)
	{
		temp[0] = temp[0]->next;
		el++;
	}
	temp[0] = string;
	res = (char*)malloc(el + 1);
	res[el] = 0;
	while (temp[0])
	{
		res[--el] = ((char*)(temp[0]->content))[0];
		temp[1] = temp[0];
		temp[0] = temp[0]->next;
		free(temp[1]->content);
		free(temp[1]);
	}
	return (res);
}

int		buffer_proc(t_fd *curr, t_list **rd)
{
	int		i;

	i = -1;
	if (curr->l_st[0] == 0)
		return (0);
	while (curr->l_st[++i] && curr->l_st[i] != '\n')
		ft_lstadd(rd, ft_lstnew(&curr->l_st[i], 1));
	if (curr->l_st[i] == '\n')
	{
		curr->l_st += i + 1;
		return (2);
	}
	else
		curr->l_st += i;
	return (1);
}

int		get_next_line(int fd, char **str)
{
	static t_fd *fd_s;
	t_fd		*curr;
	t_list		*rd;
	int			res[5];

	if (fd < 0 || str == NULL)
		return (-1);
	N2();
	res[0] = buffer_proc(curr, &rd);
	if (res[0] != 2)
		while ((res[1] = read(fd, curr->buff, BUFF_SIZE)))
		{
			if (res[1] == -1)
				return (-1);
			N1();
			while (++res[2] < res[1] && curr->buff[res[2]] != '\n')
				ft_lstadd(&rd, ft_lstnew(&curr->buff[res[2]], 1));
			curr->l_st = ft_strdup("");
			if (curr->buff[res[2]] == '\n')
				curr->l_st = ft_strdup(ft_strchr(curr->buff, '\n') + 1);
			if (curr->buff[res[2]] == '\n')
				break ;
		}
	*str = compose(rd);
	return (((!res[1] && !res[0] && !rd)) ? 0 : 1);
}
