/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/10 19:41:14 by elyahove          #+#    #+#             */
/*   Updated: 2016/12/21 16:41:10 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __GET_NEXT_LINE_H
# define __GET_NEXT_LINE_H
# define BUFF_SIZE 5
# include "libft.h"
# include <fcntl.h>
# define N1() (curr->buff[res[1]] = 0, res[2] = -1)
# define N2() (rd = NULL, curr = find_fd(fd, &fd_s), res[3] = fd)

typedef struct		s_fd
{
	char			*l_st;
	char			buff[BUFF_SIZE + 1];
	int				fd;
	struct s_fd		*next;
}					t_fd;

int					get_next_line(int fd, char **str);

#endif
