/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_darr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 20:13:52 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/29 20:20:43 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_darr(size_t len, char c)
{
	char	**fin;
	size_t	i;
	size_t	j;

	fin = (char**)malloc(len + 1);
	if (fin == NULL)
		return (NULL);
	fin[len][0] = 0;
	i = 0;
	while (i < len)
	{
		j = 0;
		fin[i] = malloc(len + 1);
		if (fin[i] == NULL)
			return (NULL);
		fin[i][len] = 0;
		while (j < len)
		{
			fin[i][j] = c;
			j++;
		}
		i++;
	}
	return (fin);
}
