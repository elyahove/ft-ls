/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 22:04:47 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/24 19:37:19 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	size_t	dlen;
	size_t	slen;

	dlen = ft_strlen(dst);
	slen = ft_strlen(src);
	j = 0;
	i = 0;
	while (dst[j])
		j++;
	if (j > size)
		return (slen + size);
	while (src[i] && (j < size - 1))
	{
		dst[j] = src[i];
		j++;
		i++;
	}
	dst[j] = 0;
	return (slen + dlen);
}
