/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reverse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 19:01:08 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/29 19:10:28 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_reverse(char *str)
{
	char	*fin;
	int		i;
	int		j;

	if (str == NULL)
		return (NULL);
	i = ft_strlen(str);
	fin = (char *)malloc(i + 1);
	fin[i] = 0;
	j = 0;
	while (--i >= 0)
	{
		fin[j] = str[i];
		j++;
	}
	return (fin);
}
