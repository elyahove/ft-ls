/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 18:42:47 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 16:52:35 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*t;
	int		fl;

	fl = 0;
	t = (char *)s;
	while (*t)
	{
		if (*t == (char)c)
		{
			fl = 1;
			s = t;
		}
		t++;
	}
	if (!(*t) && c == 0)
	{
		s = t;
		return ((char *)s);
	}
	if (fl)
		return ((char *)s);
	return (NULL);
}
