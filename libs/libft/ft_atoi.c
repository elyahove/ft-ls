/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 19:33:06 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 22:05:09 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		flag[3];
	int		i;

	flag[0] = 0;
	flag[1] = 1;
	flag[2] = 1;
	i = -1;
	while ((str[++i] <= 57 && str[i] >= 48)
		|| ((str[i] == '\n' || str[i] == '\t'
		|| str[i] == ' ' || str[i] == '\r' || str[i] == '\f' || str[i] == '\v')
		&& flag[1]) || (str[i] == 45 && flag[1])
		|| (str[i] == 43 && flag[1]))
	{
		if (str[i] == '\n' || str[i] == '\t' || str[i] == ' '
			|| str[i] == '\r' || str[i] == '\v' || str[i] == '\f')
			continue ;
		else if (str[i] == 45)
			flag[2] = -1;
		else if ((str[i] <= 57 && str[i] > 48) || (str[i] == 48 && !(flag[1])))
			flag[0] = flag[0] * 10 + str[i] - 48;
		flag[1] = 0;
	}
	return (flag[0] * flag[2]);
}
