/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 18:17:44 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 19:18:59 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	is_ndelim(char ch, char del)
{
	if (ch != del)
		return (1);
	return (0);
}

static char	**actual_split(char const *s, char c, char **res)
{
	int		i;
	int		j;
	int		word;

	word = 0;
	i = -1;
	while (s[++i])
	{
		if ((is_ndelim(s[i], c) && !i)
			|| (is_ndelim(s[i], c) && !is_ndelim(s[i - 1], c)))
		{
			j = 0;
			while (is_ndelim(s[i + j], c) && s[i + j])
				j++;
			res[word] = (char*)malloc(j);
			if (res[word] == NULL)
				return (NULL);
			res[word][j] = 0;
			while (--j != -1)
				res[word][j] = s[i + j];
			word++;
		}
	}
	return (res);
}

char		**ft_strsplit(char const *s, char c)
{
	int		i;
	int		len;
	char	**split;

	i = -1;
	len = 0;
	if (s == NULL)
		return (NULL);
	while (s[++i])
		if ((is_ndelim(s[i], c) && !i)
			|| (is_ndelim(s[i], c) && !is_ndelim(s[i - 1], c)))
			len++;
	split = (char **)malloc(sizeof(char *) * (len + 1));
	if (split == NULL)
		return (NULL);
	split[len] = NULL;
	split = actual_split(s, c, split);
	return (split);
}
