/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 19:14:17 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 19:04:24 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	count_nums(int n, int *fl)
{
	int		nums;

	if (n == 0)
		return (1);
	nums = 0;
	while (n)
	{
		n /= 10;
		nums++;
	}
	if (fl[1])
		nums++;
	if (fl[0])
		nums++;
	return (nums);
}

static char	*convert(int n, char *res, int *fl)
{
	res[fl[2]] = 0;
	fl[2]--;
	while (fl[2] != -1)
	{
		if (fl[1] && fl[2] == 0)
		{
			fl[2]--;
			continue ;
		}
		if (fl[2] == 10 && fl[0])
		{
			res[fl[2]] = '8';
			fl[2]--;
			continue ;
		}
		res[fl[2]] = (n % 10) + 48;
		n = n / 10;
		fl[2]--;
	}
	if (fl[1])
		res[0] = '-';
	return (res);
}

char		*ft_itoa(int n)
{
	int		fl[3];
	char	*res;

	fl[0] = 0;
	fl[1] = 0;
	if (n == -2147483648)
	{
		fl[0] = 1;
		n /= 10;
	}
	if (n < 0)
	{
		fl[1] = 1;
		n = -n;
	}
	fl[2] = count_nums(n, fl);
	res = (char *)malloc(fl[2] + 1);
	if (res == NULL)
		return (NULL);
	res = convert(n, res, fl);
	return (res);
}
