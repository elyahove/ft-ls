/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 18:51:04 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 17:00:45 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	char		*res;
	size_t		i;
	size_t		j;

	i = 0;
	res = NULL;
	if (*little == 0)
		return ((char *)big);
	while (big[i])
	{
		if (big[i] == little[0])
		{
			j = 0;
			while ((big[i + j] == little[j]) && big[i + j] && little[j])
				j++;
			if (little[j] == 0)
			{
				res = ((char *)big + i);
				return (res);
			}
		}
		i++;
	}
	return (res);
}
