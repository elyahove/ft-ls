/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 18:17:53 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 21:59:33 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*res;

	res = malloc(sizeof(t_list));
	if (res == NULL)
		return (NULL);
	res->content = malloc(content_size);
	res->next = NULL;
	res->content_size = (unsigned int)malloc(sizeof(size_t));
	if (content != NULL)
		ft_memcpy(res->content, content, content_size);
	else
		res->content = NULL;
	content_size = 0;
	res->content_size = content_size;
	return (res);
}
