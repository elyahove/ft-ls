/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 19:03:56 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 17:02:02 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	char		*res;
	size_t		i;
	size_t		j;

	i = 0;
	res = NULL;
	if (*little == 0)
		return ((char *)big);
	while (big[i] && i < len)
	{
		if (big[i] == little[0])
		{
			j = 0;
			while ((big[i + j] == little[j]) && big[i + j] && little[j])
				j++;
			if (little[j] == 0 && ((i + j) <= len))
			{
				res = ((char *)big + i);
				return (res);
			}
		}
		i++;
	}
	return (res);
}
