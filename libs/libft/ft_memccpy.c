/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 16:49:45 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/23 17:26:48 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		*((unsigned char *)dst) = *((unsigned char *)src);
		if (*((unsigned char *)dst) == (unsigned char)c)
			return (++dst);
		dst++;
		src++;
		i++;
	}
	return (NULL);
}
