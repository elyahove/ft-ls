/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtoupper.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 18:28:30 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/29 18:32:59 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtoup(char *str)
{
	size_t		i;
	char		*fin;

	fin = (char *)malloc(ft_strlen(str) + 1);
	fin[ft_strlen(str)] = 0;
	i = 0;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			fin[i] = str[i] - 32;
		else
			fin[i] = str[i];
		i++;
	}
	return (fin);
}
