/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 18:21:28 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 22:07:17 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(void *, size_t))
{
	t_list		*ptr;

	ptr = *alst;
	*alst = (*alst)->next;
	(*del)(ptr->content, ptr->content_size);
	ft_memdel((void **)&ptr);
}
