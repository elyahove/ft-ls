/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_capitalize.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 18:40:19 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/29 19:26:32 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*transform(char *src, char *dst, size_t i, int new)
{
	while (src[i])
	{
		if (src[i] == '.' || src[i] == '!' || src[i] == '?')
			new = 0;
		else if (src[i] >= 'A' && src[i] <= 'Z' && new == 0)
			new = 1;
		else if (src[i] >= 'a' && src[i] <= 'z' && new == 0)
		{
			dst[i] = src[i] - 32;
			new = 1;
			i++;
			continue ;
		}
		else if (src[i] >= 'A' && src[i] <= 'Z' && new == 1)
		{
			dst[i] = src[i] + 32;
			i++;
			continue ;
		}
		dst[i] = src[i];
		i++;
	}
	return (dst);
}

char		*ft_capitalize(char *str)
{
	char		*fin;

	fin = (char *)malloc(ft_strlen(str) + 1);
	fin[ft_strlen(str)] = 0;
	fin = transform(str, fin, 0, 0);
	return (fin);
}
