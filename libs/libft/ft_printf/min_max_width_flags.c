/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   min_max_width_flags.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 17:47:18 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:25:08 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		to_next_integer(const char *format, int i)
{
	int		j;

	j = 0;
	if (format[i] == '0' && valid_zero_flag(format, i))
		return (1);
	while (!is_specifier(format[j + i]))
	{
		if (format[j + i] < '0' || format[j + i] > '9')
			return (j);
		j++;
	}
	return (0);
}

int		find_and_return_min(const char *format)
{
	int		start;
	int		res;
	int		tmp;

	start = 0;
	res = 0;
	while (!is_specifier(format[start]))
	{
		tmp = find_and_norm(format, &start, &res);
		if (tmp == 0)
			return (res);
		else if (tmp == 2)
			continue ;
		if (!to_next_integer(format, start))
		{
			if (format[start] > '0' && format[start] <= '9')
				return (res);
			start++;
		}
		else
			start += to_next_integer(format, start);
	}
	return (res);
}

void	assign_flags(const char *format, t_format **current)
{
	int			i;
	t_format	*curr;

	curr = *current;
	i = 0;
	while (!is_specifier(format[i]))
	{
		if (format[i] == '#' && !(curr->flag & 0x01))
			curr->flag += HASH;
		if (format[i] == ' ' && !(curr->flag & 0x02))
			curr->flag += SPACE;
		if (format[i] == '-' && !(curr->flag & 0x04))
			curr->flag += MINUS;
		if (format[i] == '+' && !(curr->flag & 0x08))
			curr->flag += PLUS;
		if (valid_zero_flag(format, i) && !(curr->flag & 0x10))
			curr->flag += ZERO;
		i++;
	}
}

void	assign_min_max_width(const char *format, t_format **current)
{
	int			i;
	t_format	*curr;

	curr = *current;
	curr->width = find_and_return_min(format);
	i = 0;
	while (!is_specifier(format[i]))
	{
		if (format[i] == '.')
		{
			curr->precision = ft_printf_atoi(format + i + 1);
			i += to_next_integer(format, i + 1);
		}
		i++;
	}
}
