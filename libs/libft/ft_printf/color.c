/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 20:38:13 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:27:36 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	**return_colors(void)
{
	char	**res;

	res = (char**)malloc(sizeof(char*) * 7);
	res[0] = RED;
	res[1] = GREEN;
	res[2] = YELLOW;
	res[3] = BLUE;
	res[4] = MAGENTA;
	res[5] = CYAN;
	res[6] = RESET;
	return (res);
}

int		valid_color(char *color)
{
	if (!ft_strcmp(color, "red"))
		return (1);
	else if (!ft_strcmp(color, "green"))
		return (2);
	else if (!ft_strcmp(color, "yellow"))
		return (3);
	else if (!ft_strcmp(color, "blue"))
		return (4);
	else if (!ft_strcmp(color, "magenta"))
		return (5);
	else if (!ft_strcmp(color, "cyan"))
		return (6);
	else if (!ft_strcmp(color, "eoc"))
		return (7);
	return (0);
}

void	check_color(t_params **p, int i, int j)
{
	char		**res;

	res = return_colors();
	if (j)
	{
		write(1, res[j - 1], ft_printf_strlen(res[j - 1]));
		(*p)->iter += i + 1;
		(*p)->printed += ft_printf_strlen(res[j - 1]);
	}
	else
	{
		write(1, "{", 1);
		(*p)->printed++;
	}
}

void	manage_color(char const *format, t_params **p)
{
	char	current[8];
	int		i;
	int		j;

	i = 0;
	current[7] = 0;
	(*p)->iter++;
	while (i < 7 && format[(*p)->iter + i] != '}'
			&& format[(*p)->iter + i])
	{
		current[i] = format[(*p)->iter + i];
		i++;
	}
	current[i] = 0;
	j = valid_color(current);
	check_color(p, i, j);
}
