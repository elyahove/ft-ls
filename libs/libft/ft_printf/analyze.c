/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analyze.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 19:56:01 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 18:37:25 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_specifier(char c)
{
	if (c == 's' || c == 'S' || c == 'p' || c == 'd'
			|| c == 'D' || c == 'i' || c == 'o' || c == 'O'
			|| c == 'u' || c == 'U' || c == 'x' || c == 'X'
			|| c == 'c' || c == 'C' || c == '%')
		return (1);
	return (0);
}

int		is_valid_symbol(char c)
{
	if (c == '#' || c == ' ' || c == '-' || c == '+'
		|| (c >= '0' && c <= '9') || c == '*'
		|| c == '.' || c == 'l' || c == 'h' || c == 'L' || c == 'j'
		|| c == 'z' || c == '$')
		return (1);
	return (0);
}

int		is_valid_specifier(const char *format)
{
	int		i;

	i = 0;
	while (!is_specifier(format[i]))
	{
		if (!is_valid_symbol(format[i]))
			return (0);
		i++;
	}
	return (1);
}

void	analyze(const char *format, t_params **p)
{
	t_params	*params;
	int			i;

	params = *p;
	(params->iter)++;
	i = params->iter;
	if (format[i] == 0)
		return ;
	if (!is_valid_specifier(format + i))
	{
		(*p)->iter += chars_to_next_format(format + i);
		return ;
	}
	assign_flags(format + i, &(params->frmt));
	assign_min_max_width(format + i, &(params->frmt));
	assign_type_len(format + i, &params->frmt);
	assign_type_spec(format + i, &params->frmt);
	assign_argument(format + i, &params);
	print_analyze(params->frmt, p);
	nullify_format(params->frmt);
	params->iter += chars_to_next_format(format + i);
}
