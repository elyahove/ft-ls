/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norm.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/11 18:35:30 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:27:53 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	itoa_norm(intmax_t num, size_t len, char **res)
{
	if (num < 0)
	{
		num = -num;
		(*res)[0] = '-';
	}
	while (num)
	{
		(*res)[--len] = (num % 10) + 48;
		num /= 10;
	}
}

void	assign_norm(t_params **current)
{
	if ((*current)->frmt->arg == NULL)
	{
		(*current)->frmt->arg = (*current)->curr;
		if ((*current)->frmt->arg)
			(*current)->curr = (*current)->frmt->arg->next;
	}
}

int		find_and_norm(char const *format, int *start, int *res)
{
	if ((format[*start] > '0' && format[*start] <= '9')
		|| (format[*start] == '0' && !valid_zero_flag(format, *start)))
	{
		if (format[*start - 1] == '.' || (format[*start - 1] == '0'
					&& !valid_zero_flag(format, *start - 1)))
		{
			if (!to_next_integer(format, *start))
				return (0);
			*start += to_next_integer(format, *start);
			return (2);
		}
		if (format[*start + to_next_integer(format, *start)] == '$')
		{
			*start += to_next_integer(format, *start);
			return (2);
		}
		*res = ft_printf_atoi(format + *start);
	}
	return (1);
}
