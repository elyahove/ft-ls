/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   specs_conditionals.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 16:13:46 by elyahove          #+#    #+#             */
/*   Updated: 2017/02/10 18:50:33 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_octal(char c)
{
	if (c == 'o' || c == 'O')
		return (1);
	return (0);
}

int		is_octal_hex(char c)
{
	if (c == 'x' || c == 'X' || is_octal(c))
		return (1);
	return (0);
}

int		has_function_in_arr(char c)
{
	if (c == 'd' || c == 'D' || c == 'u'
		|| c == 'U' || c == 'i' || c == 'p'
		|| c == 'x' || c == 'X' || c == 'o'
		|| c == 'O' || c == 'c' || c == 'C')
		return (1);
	return (0);
}

int		is_integer_spec(char c)
{
	if (c == 'd' || c == 'D' || c == 'u' || c == 'U' || c == 'i'
			|| c == 'x' || c == 'X' || c == 'o' || c == 'O' || c == 'p')
		return (1);
	return (0);
}

int		is_signed_spec(char c)
{
	if (c == 'd' || c == 'i' || c == 'D')
		return (1);
	return (0);
}
