/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   types.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 15:40:16 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:26:20 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*char_to_str(t_format *f, t_params **p)
{
	char	*res;

	(void)p;
	res = (char*)malloc(sizeof(char) * 2);
	res[1] = 0;
	res[0] = (char)f->arg->arg;
	return (res);
}

intmax_t	return_type(void *arg, int flag)
{
	intmax_t	res;

	res = (intmax_t)arg;
	if (flag == 0)
		res = (int)res;
	if (flag == 1)
		res = (char)res;
	if (flag == 2)
		res = (short)res;
	if (flag == 3)
		res = (long)res;
	if (flag == 4)
		res = (long long)res;
	if (flag == 5)
		res = (intmax_t)res;
	if (flag == 6)
		res = (ssize_t)res;
	return (res);
}

uintmax_t	return_utype(void *arg, int flag)
{
	uintmax_t	res;

	res = (uintmax_t)arg;
	if (flag == 0)
		res = (unsigned int)res;
	if (flag == 1)
		res = (unsigned char)res;
	if (flag == 2)
		res = (unsigned short)res;
	if (flag == 3)
		res = (unsigned long)res;
	if (flag == 4)
		res = (unsigned long long)res;
	if (flag == 5)
		res = (uintmax_t)res;
	if (flag == 6)
		res = (size_t)res;
	return (res);
}

t_arg		*find_arg_by_num(t_arg *start, int num)
{
	t_arg	*curr;

	curr = start;
	while (curr)
	{
		if (curr->num == num - 1)
			return (curr);
		curr = curr->next;
	}
	return (NULL);
}

void		assign_argument(const char *format, t_params **current)
{
	int				i;
	int				num;

	if ((*current)->frmt->spec == '%')
		return ;
	i = 0;
	while (!is_specifier(format[i]))
	{
		if (format[i] == '$' && (format[i - 1] >= '0' && format[i - 1] <= '9'))
		{
			i--;
			while (format[i] >= '0' && format[i] <= '9')
				i--;
			if (format[i++] == '0')
				i++;
			num = ft_printf_atoi(format + i);
			(*current)->frmt->arg = find_arg_by_num((*current)->start, num);
			if ((*current)->frmt->arg->next)
				(*current)->curr = (*current)->frmt->arg->next;
			break ;
		}
		i++;
	}
	assign_norm(current);
}
