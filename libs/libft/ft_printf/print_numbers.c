/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_numbers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 19:23:18 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:26:33 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	support_signs(t_format *f, t_params **p)
{
	if (is_signed_spec(f->spec))
	{
		if (has_flag(f, PLUS) && !f->neg)
		{
			write(1, "+", 1);
			(*p)->printed++;
		}
		else if (has_flag(f, SPACE) && !f->neg)
		{
			write(1, " ", 1);
			(*p)->printed++;
		}
		if (!has_flag(f, MINUS) && f->neg)
		{
			write(1, "-", 1);
			(*p)->printed++;
		}
	}
}

void	print_number(t_format *f, t_params **p)
{
	int		i;

	if ((has_flag(f, MINUS)) || (!has_flag(f, MINUS) && !has_flag(f, ZERO)))
		support_signs(f, p);
	if (has_flag(f, MINUS) || (!has_flag(f, MINUS) && !has_flag(f, ZERO)))
		print_hex_octal(f, p);
	if (has_flag(f, MINUS) && f->neg)
	{
		write(1, "-", 1);
		(*p)->printed++;
	}
	if (f->precision > f->strl)
	{
		i = 0;
		while (f->strl + i++ < f->precision)
			write(1, "0", 1);
		(*p)->printed += i - 1;
	}
	ft_printf_putstr(f->f_arg, f->strl);
	free(f->neg ? f->f_arg - 1 : f->f_arg);
	(*p)->printed += (f->strl);
}
