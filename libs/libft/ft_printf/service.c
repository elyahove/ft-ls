/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   service.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 18:48:46 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:25:53 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	assign_functions(t_conv_func *f)
{
	f[(int)'d'] = &ft_printf_itoa;
	f[(int)'i'] = &ft_printf_itoa;
	f[(int)'D'] = &ft_printf_itoa;
	f[(int)'x'] = &itoa_hex;
	f[(int)'p'] = &itoa_hex;
	f[(int)'X'] = &itoa_hex;
	f[(int)'o'] = &itoa_octal;
	f[(int)'O'] = &itoa_octal;
	f[(int)'u'] = &ft_uitoa;
	f[(int)'U'] = &ft_uitoa;
	f[(int)'c'] = &char_to_str;
	f[(int)'C'] = &char_to_str;
}

int		valid_zero_flag(char const *format, int i)
{
	if (format[i] != '0')
		return (0);
	if (i > 0)
	{
		if ((format[i - 1] >= '1' && format[i - 1] <= '9')
				|| format[i - 1] == '.')
			return (0);
	}
	return (1);
}

int		chars_to_next_format(char const *format)
{
	int			i;

	i = 0;
	while (!is_specifier(format[i]) && format[i])
	{
		if (!is_valid_symbol(format[i]))
			return (i);
		i++;
	}
	if (!format[i])
		return (0);
	return (++i);
}

int		count_specifiers(char const *format)
{
	size_t		i;
	int			qnt;

	i = 0;
	qnt = 0;
	while (format[i])
	{
		if (format[i] == '%')
		{
			qnt++;
			i += chars_to_next_format(format + i + 1) + 1;
			if (format[i - 1] == '%' || !is_specifier(format[i - 1]))
			{
				qnt--;
				continue ;
			}
		}
		else
			i++;
	}
	return (qnt);
}

t_arg	*assign_vars(va_list args, int count, int num)
{
	t_arg	*curr;

	if (count == 0)
		return (NULL);
	curr = (t_arg*)malloc(sizeof(t_arg));
	curr->arg = va_arg(args, void*);
	curr->num = num;
	curr->next = assign_vars(args, count - 1, num + 1);
	return (curr);
}
