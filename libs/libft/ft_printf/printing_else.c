/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing_else.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 20:15:05 by elyahove          #+#    #+#             */
/*   Updated: 2017/02/10 18:42:25 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_hex_octal(t_format *f, t_params **p)
{
	if (!is_octal_hex(f->spec) && f->spec != 'p')
		return ;
	if (f->spec == 'p')
	{
		write(1, "0x", 2);
		(*p)->printed += 2;
	}
	if (has_flag(f, HASH) && f->f_arg[0] != '0')
	{
		if (is_octal(f->spec) || ((f->spec == 'x' || f->spec == 'X')
			&& f->precision != 0))
		{
			write(1, "0", 1);
			(*p)->printed++;
			if (((f->spec == 'x' || f->spec == 'X') && f->precision != 0))
			{
				write(1, &f->spec, 1);
				(*p)->printed++;
			}
		}
	}
}

void	string_print(t_format *f, t_params **p)
{
	int		i;
	int		temp;

	i = 0;
	if (f->precision != -1)
		temp = f->strl < f->precision ? f->strl : f->precision;
	else
		temp = f->strl;
	if (temp < f->width)
	{
		while (i < f->width - temp)
		{
			if (f->precision <= 0 && has_flag(f, ZERO))
				write(1, "0", 1);
			else
				write(1, " ", 1);
			(*p)->printed++;
			i++;
		}
	}
}

void	integer_print(t_format *f, t_params **p)
{
	int		i;

	i = 0;
	if (((is_octal_hex(f->spec) || f->spec == 'p') && (!has_flag(f, MINUS)
			&& has_flag(f, ZERO))))
		print_hex_octal(f, p);
	if (!has_flag(f, MINUS) && (has_flag(f, ZERO)))
		support_signs(f, p);
	if (f->precision > 0 && f->precision > f->strl)
	{
		while (i++ < f->width - f->precision)
			write(1, " ", 1);
	}
	else
	{
		if (has_flag(f, ZERO) && f->precision < f->strl)
			while (i++ < f->width - f->strl)
				has_flag(f, MINUS) ? write(1, " ", 1) : write(1, "0", 1);
		else
			while (i++ < f->width - f->strl)
				write(1, " ", 1);
	}
	(*p)->printed += i - 1;
}

void	print_else_assist(t_format *f, t_params **p)
{
	int		i;

	if (f->spec == 'c' || f->spec == '%' || f->spec == 0)
	{
		i = 0;
		while (i++ < f->width - 1)
		{
			has_flag(f, ZERO) ? write(1, "0", 1) : write(1, " ", 1);
			(*p)->printed++;
		}
	}
	if (f->spec == 's' || f->spec == 'S')
	{
		string_print(f, p);
	}
}

void	print_else(t_format *f, t_params **p)
{
	if (is_integer_spec(f->spec))
	{
		if (is_signed_spec(f->spec))
		{
			if ((has_flag(f, PLUS) || has_flag(f, SPACE))
				&& !f->neg)
				f->width--;
			if (f->neg)
				f->width--;
		}
		if ((is_octal_hex(f->spec) && has_flag(f, HASH)
			&& f->f_arg[0] != '0') || f->spec == 'p')
		{
			f->width--;
			if (f->spec == 'x' || f->spec == 'X' || f->spec == 'p')
				f->width--;
		}
		integer_print(f, p);
	}
	else
		print_else_assist(f, p);
}
