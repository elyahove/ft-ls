/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_format.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 20:08:03 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:21:55 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_printf_bzero(void *s, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		((unsigned char*)s)[i] = 0;
		i++;
	}
}

size_t	octal_len(uintmax_t num)
{
	size_t	len;

	if (num == 0)
		return (1);
	len = 0;
	while (num)
	{
		num /= 8;
		len++;
	}
	return (len);
}

char	*itoa_octal(t_format *arg, t_params **p)
{
	char		*base;
	uintmax_t	num;
	char		*res;
	size_t		len;

	(void)p;
	base = "01234567";
	num = return_utype(arg->arg->arg, arg->len_type);
	if (num == 0)
		return (ft_strdup("0"));
	len = octal_len(num);
	res = (char*)malloc(len + 1);
	res[len] = 0;
	while (num)
	{
		res[--len] = base[num % 8];
		num /= 8;
	}
	return (res);
}
