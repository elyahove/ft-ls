/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing_functions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 20:16:07 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:29:07 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ft_printf_strlen(const char *str)
{
	size_t	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	ft_printf_putstr(char *str, int lim)
{
	int		i;

	i = 0;
	while (str[i] && i < lim)
		write(1, &str[i++], 1);
}

void	support_print_arg(t_format *f, t_params **p)
{
	if (f->spec == '%' || f->spec == 'c' || f->spec == 'C')
	{
		if (f->f_arg[0] == '\0')
			write(1, "\0", 1);
		else
			write(1, f->f_arg, 1);
		free(f->f_arg);
		(*p)->printed++;
	}
	if (f->spec == 's' || f->spec == 'S')
	{
		if (f->precision < f->strl && f->precision != -1)
		{
			ft_printf_putstr(f->f_arg, f->precision);
			(*p)->printed += f->precision;
		}
		else
		{
			ft_printf_putstr(f->f_arg, f->strl);
			(*p)->printed += f->strl;
		}
		free(f->f_arg);
	}
}

void	print_argument(t_format *f, t_params **p)
{
	if (is_integer_spec(f->spec))
		print_number(f, p);
	else
		support_print_arg(f, p);
}

void	print_analyze(t_format *f, t_params **p)
{
	get_formatted_arg(f, p);
	if (f->f_arg[0] == '-' && is_integer_spec(f->spec))
	{
		f->neg = 1;
		f->f_arg++;
	}
	f->strl = ft_printf_strlen(f->f_arg);
	if (f->flag & 0x04)
	{
		print_argument(f, p);
		print_else(f, p);
	}
	else
	{
		print_else(f, p);
		print_argument(f, p);
	}
}
