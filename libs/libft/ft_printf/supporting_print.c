/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   supporting_print.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 18:32:38 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:26:58 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	get_w_help(wchar_t hidden, int *len, char **res)
{
	(*res)[0] = (unsigned char)(((hidden << 6) | 0xF0000000) >> 24);
	(*res)[1] = (unsigned char)(((hidden << 4) >> 16) | 0x80);
	(*res)[2] = (unsigned char)((((hidden << 2) & 0x3FBF) >> 8) | 0x80);
	(*res)[3] = (unsigned char)((hidden & 0xBF) | 0x80);
	*len = 4;
}

char	*get_w_char(wchar_t hidden, int *len, int p)
{
	char	*res;

	p = (p < 0) ? 5 : p;
	PREPARE;
	if (hidden <= 0x7F)
	{
		res[0] = (unsigned char)hidden;
		*len = 1;
	}
	else if (hidden <= 0x7FF && p > 1)
	{
		res[0] = (unsigned char)(((hidden << 2) | 0xC000) >> 8);
		res[1] = (unsigned char)((hidden & 0xBF) | 0x80);
		*len = 2;
	}
	else if (hidden <= 0xFFFF && p > 2)
	{
		res[0] = (unsigned char)(((hidden << 4) | 0xE00000) >> 16);
		res[1] = (unsigned char)((((hidden << 2) & 0x3FBF) >> 8) | 0x80);
		res[2] = (unsigned char)((hidden & 0xBF) | 0x80);
		*len = 3;
	}
	else if (hidden <= 0x1FFFFF && p > 3)
		get_w_help(hidden, len, &res);
	return (res);
}

char	*get_w_string(t_format *f, t_params **p)
{
	char		*res;
	wchar_t		*str;
	size_t		len;
	int			*clen;

	(void)p;
	str = (int*)f->arg->arg;
	len = ft_printf_strlen((char*)str);
	res = (char *)malloc(1);
	res[0] = 0;
	clen = (int *)malloc(sizeof(int) * 3);
	clen[0] = 0;
	clen[1] = 0;
	while (*str)
	{
		res = ft_strjoin(res, get_w_char((wchar_t)(*str),
			&clen[1],
			(f->precision == -1 ? -1 : f->precision - clen[0])));
		clen[0] += clen[1];
		str++;
	}
	return (res);
}

void	check_null(t_format *f, t_params **p)
{
	if (f->arg->arg == NULL)
		f->f_arg = ft_strdup("(null)");
	else
	{
		if (f->spec == 'S' || (f->spec == 's' && f->len_type == 3))
			f->f_arg = get_w_string(f, p);
		else
			f->f_arg = ft_strdup((char*)f->arg->arg);
	}
}

void	get_formatted_arg(t_format *f, t_params **p)
{
	if (has_function_in_arr(f->spec))
		f->f_arg = (*p)->func[(int)f->spec](f, p);
	else if (f->spec == 's' || f->spec == 'S')
		check_null(f, p);
	else if (f->spec == '%')
		f->f_arg = ft_strdup("%");
	if (((f->spec == 'c' || f->spec == 'C') && f->arg->arg == NULL)
		|| (f->precision == 0 && (f->f_arg[0] == '0'
				&& is_integer_spec(f->spec))))
		f->f_arg = ft_strdup("");
}
