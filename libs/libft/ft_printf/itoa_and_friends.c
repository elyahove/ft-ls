/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 19:33:06 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:21:35 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t		len_itoa(intmax_t num)
{
	size_t	len;

	len = 0;
	if (num == 0)
		return (0);
	while (num)
	{
		len++;
		num /= 10;
	}
	return (len);
}

size_t		len_uitoa(uintmax_t num)
{
	size_t	len;

	len = 0;
	if (num == 0)
		return (0);
	while (num)
	{
		len++;
		num /= 10;
	}
	return (len);
}

char		*ft_uitoa(t_format *f, t_params **p)
{
	uintmax_t	num;
	size_t		len;
	char		*res;

	(void)p;
	num = return_utype(f->arg->arg, f->len_type);
	if (num == 0)
		return (ft_strdup("0"));
	len = len_uitoa(num);
	res = (char*)malloc(len + 1);
	res[len] = 0;
	while (num)
	{
		res[--len] = (num % 10) + 48;
		num /= 10;
	}
	return (res);
}

char		*ft_printf_itoa(t_format *f, t_params **p)
{
	intmax_t	num;
	size_t		len;
	char		*res;

	(void)p;
	if (f->spec == 'u' || f->spec == 'U')
		return (ft_uitoa(f, p));
	else
	{
		num = return_type(f->arg->arg, f->len_type);
		len = len_itoa(num);
		if (num == 0)
			return (ft_strdup("0"));
		if ((long long)num / 10 == -922337203685477580 && num % 10 == -8)
			return (ft_strdup("-9223372036854775808"));
		if (num < 0)
			len++;
		res = (char *)malloc(len + 1);
		res[len] = 0;
		itoa_norm(num, len, &res);
		return (res);
	}
}

int			ft_printf_atoi(const char *str)
{
	int		flag[3];
	int		i;

	flag[0] = 0;
	flag[1] = 1;
	flag[2] = 1;
	i = -1;
	while ((str[++i] <= 57 && str[i] >= 48)
		|| ((str[i] == '\n' || str[i] == '\t'
		|| str[i] == ' ' || str[i] == '\r' || str[i] == '\f' || str[i] == '\v')
		&& flag[1]) || (str[i] == 45 && flag[1])
		|| (str[i] == 43 && flag[1]))
	{
		if (str[i] == '\n' || str[i] == '\t' || str[i] == ' '
			|| str[i] == '\r' || str[i] == '\v' || str[i] == '\f')
			continue ;
		else if (str[i] == 45)
			flag[2] = -1;
		else if ((str[i] <= 57 && str[i] > 48) || (str[i] == 48 && !(flag[1])))
			flag[0] = flag[0] * 10 + str[i] - 48;
		flag[1] = 0;
	}
	return (flag[0] * flag[2]);
}
