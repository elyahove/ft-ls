/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   length_format.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/29 14:11:43 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 18:08:01 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_valid_type_len(char c)
{
	if (c == 'l' || c == 'h' || c == 'j' || c == 'z')
		return (1);
	return (0);
}

int		return_type_len(const char *format, int i)
{
	if (format[i] == 'h' && format[i + 1] == 'h')
		return (1);
	else if (format[i] == 'h')
		return (2);
	else if (format[i] == 'l' && format[i + 1] == 'l')
		return (4);
	else if (format[i] == 'l')
		return (3);
	else if (format[i] == 'j')
		return (5);
	else if (format[i] == 'z')
		return (6);
	return (0);
}

void	assign_type_len(const char *format, t_format **current)
{
	int			i;
	t_format	*curr;
	int			type_len;

	curr = *current;
	i = 0;
	while (!is_specifier(format[i]))
	{
		if (is_valid_type_len(format[i]))
		{
			type_len = return_type_len(format, i);
			if (curr->len_type < type_len)
				curr->len_type = type_len;
			if (type_len == 1 || type_len == 4)
				i += 2;
			else
				i++;
			continue ;
		}
		i++;
	}
}

void	assign_type_spec(const char *format, t_format **current)
{
	int			i;
	t_format	*curr;

	curr = *current;
	i = 0;
	while (!is_specifier(format[i]))
		i++;
	curr->spec = format[i];
	if (format[i] == 'U' || format[i] == 'D' || format[i] == 'O')
	{
		curr->spec = format[i] + 32;
		curr->len_type = 3;
	}
	if (format[i] == 'p')
		curr->len_type = 3;
}
