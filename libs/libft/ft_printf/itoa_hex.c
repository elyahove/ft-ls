/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_hex.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 17:40:20 by elyahove          #+#    #+#             */
/*   Updated: 2017/02/10 18:36:11 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	hex_len(uintmax_t num)
{
	size_t	len;

	len = 0;
	if (num == 0)
		return (1);
	while (num)
	{
		len++;
		num /= 16;
	}
	return (len);
}

char	*itoa_hex(t_format *arg, t_params **p)
{
	char		*base;
	char		*res;
	uintmax_t	num;
	size_t		len_iter;

	(void)p;
	num = return_utype(arg->arg->arg, arg->len_type);
	if (num == 0)
		return (ft_strdup("0"));
	base = (arg->spec == 'X' ? "0123456789ABCDEF" : "0123456789abcdef");
	len_iter = hex_len(num);
	res = (char *)malloc(len_iter + 1);
	res[len_iter] = 0;
	while (--len_iter)
	{
		res[len_iter] = base[num % 16];
		num /= 16;
	}
	res[len_iter] = base[num % 16];
	return (res);
}
