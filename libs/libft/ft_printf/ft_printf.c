/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/20 20:25:21 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 19:16:48 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		nullify_format(t_format *res)
{
	res->arg = NULL;
	res->flag = 0;
	res->len_type = 0;
	res->spec = 0;
	res->precision = -1;
	res->strl = 0;
	res->neg = 0;
	res->width = 0;
	res->f_arg = NULL;
}

t_format	*create_new_format(void)
{
	t_format	*res;

	res = (t_format*)malloc(sizeof(t_format));
	nullify_format(res);
	return (res);
}

t_params	*create_params_assign_vars(const char *format, va_list args)
{
	t_params	*params;

	params = (t_params*)malloc(sizeof(t_params));
	//params->buff = (char*)malloc(sizeof(char) * 512);
	//params->buff_len = 512;
	params->frmt = create_new_format();
	params->start = assign_vars(args, count_specifiers(format), 0);
	params->curr = params->start;
	params->iter = 0;
	return (params);
}

int			clean_ft_printf(t_params *params)
{
	t_arg	*curr;
	t_arg	*temp;
	int		printed;

	curr = params->start;
	while (curr)
	{
		temp = curr;
		curr = curr->next;
		free(temp);
	}
	printed = params->printed;
	free(params);
	free(params->frmt);
	return (printed);
}

int			ft_printf(const char *format, ...)
{
	va_list		args;
	t_params	*params;

	va_start(args, format);
	params = create_params_assign_vars(format, args);
	va_end(args);
	assign_functions(params->func);
	while (format[params->iter])
	{
		if (format[params->iter] == '{')
			manage_color(format, &params);
		else if (format[params->iter] != '%')
		{
			write(1, &format[params->iter], 1);
			params->printed++;
			params->iter++;
		}
		else
			analyze(format, &params);
	}
	return (clean_ft_printf(params));
}
