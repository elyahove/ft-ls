/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 19:46:38 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/22 21:03:13 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t	count;
	size_t	length;

	count = 0;
	length = ft_strlen(src);
	while (count < len)
	{
		if (count < length)
			dst[count] = src[count];
		else
			dst[count] = 0;
		count++;
	}
	return (dst);
}
