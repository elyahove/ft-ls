/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 21:27:07 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 19:13:24 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_isspace(char c)
{
	if (c == ' ' || c == '\t' || c == '\n')
		return (1);
	return (0);
}

static char	*create_new(int i, int len, char *res, char const *s)
{
	int		ctr;

	ctr = 0;
	while (i <= len)
	{
		res[ctr] = s[i];
		ctr++;
		i++;
	}
	res[ctr] = 0;
	return (res);
}

char		*ft_strtrim(char const *s)
{
	size_t		i;
	size_t		len;
	char		*res;

	if (s == NULL)
		return (NULL);
	i = 0;
	while (ft_isspace(s[i]))
		i++;
	len = ft_strlen(s) - 1;
	if (i >= len)
	{
		res = (char *)malloc(1);
		res[0] = 0;
		return ((char *)res);
	}
	while (s[len] == ' ' || s[len] == '\t' || s[len] == '\n')
		len--;
	res = (char *)malloc(len - i + 2);
	if (res == NULL)
		return (NULL);
	res = create_new(i, len, res, s);
	return (res);
}
