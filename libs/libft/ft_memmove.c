/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 17:58:10 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/26 16:42:34 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t l)
{
	size_t		i;

	i = -1;
	if (dst < src)
		while (++i < l)
			*(unsigned char*)(dst + i) = *(unsigned char*)(src + i);
	if (dst > src)
		while (l--)
			*(unsigned char*)(dst + l) = *(unsigned char*)(src + l);
	return (dst);
}
