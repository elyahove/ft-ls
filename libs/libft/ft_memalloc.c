/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 15:01:57 by elyahove          #+#    #+#             */
/*   Updated: 2016/11/29 19:00:46 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*res;

	res = (char*)malloc(size);
	if (res == NULL)
		return (NULL);
	while (size)
	{
		res[size] = 0;
		size--;
	}
	res[size] = 0;
	return ((void*)res);
}
