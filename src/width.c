/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   width.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/14 21:17:04 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:17:28 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			nullify_widths(t_width *widths)
{
	widths->user_w = 0;
	widths->group_w = 0;
	widths->links_w = 0;
	widths->size_w = 0;
}

int				get_order_of_magn(long int num)
{
	return (num ? 1 + get_order_of_magn(num / 10) : 0);
}

void			print_spaces(int width)
{
	char	str[1024];
	int		i;

	if (width < 1)
		return ;
	i = -1;
	while (++i < width)
		str[i] = ' ';
	str[i] = 0;
	write(1, &str, width);
}

int				get_device_number_size(t_stat *curr)
{
	int		major_s;
	int		minor_s;
	int		size;

	major_s = major(curr->st_rdev);
	minor_s = minor(curr->st_rdev);
	size = get_order_of_magn(major_s) > get_order_of_magn(minor_s) ?
			get_order_of_magn(major_s) : get_order_of_magn(minor_s);
	return ((size * 2) + 2);
}

void			compare_widths(t_fs_entity *curr, t_width *widths)
{
	t_stat			info;
	int				size;
	struct passwd	*pwd;
	struct group	*grp;

	info = curr->ent_info;
	size = get_order_of_magn(info.st_nlink);
	widths->links_w = size > widths->links_w ? size : widths->links_w;
	if (S_ISCHR(curr->ent_info.st_mode) || S_ISBLK(curr->ent_info.st_mode))
		size = get_device_number_size(&curr->ent_info);
	else
		size = get_order_of_magn(info.st_size);
	widths->size_w = size > widths->size_w ? size : widths->size_w;
	size = ((grp = getgrgid(info.st_gid))) ? ft_strlen(grp->gr_name) : 0;
	widths->group_w = size > widths->group_w ? size : widths->group_w;
	size = ((pwd = getpwuid(info.st_uid))) ? ft_strlen(pwd->pw_name) : 0;
	widths->user_w = size > widths->user_w ? size : widths->user_w;
}

void			set_widths(t_fs_entity *start, t_width *widths)
{
	nullify_widths(widths);
	while (start)
	{
		compare_widths(start, widths);
		start = start->next;
	}
}
