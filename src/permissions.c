/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permissions.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 22:44:23 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/14 21:01:02 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char			user_execution_mode(int mode)
{
	if (mode & S_IXUSR)
		return ((mode & S_ISUID) ? 's' : 'x');
	return ((mode & S_ISUID) ? 'S' : '-');
}

char			group_execution_mode(int mode)
{
	if (mode & S_IXGRP)
		return ((mode & S_ISGID) ? 's' : 'x');
	return ((mode & S_ISGID) ? 'S' : '-');
}

char			sticky_bit_execution_mode(int mode)
{
	if (mode & S_ISVTX)
		return ((mode & S_IXOTH) ? 't' : 'T');
	return ((mode & S_IXOTH) ? 'x' : '-');
}

char			ext_attr_mode(char *filename)
{
	char	buf[1024];

	buf[0] = 0;
	listxattr(filename, (char*)&buf, 1024, XATTR_NOFOLLOW);
	return (buf[0] ? '@' : ' ');
}

void			print_file_mode(t_stat *ent_info, char *filename)
{
	(void)filename;
	ft_printf("%c%c%c%c%c%c%c%c%c%c%c ",
			filetype_char(ent_info->st_mode),
			(ent_info->st_mode & S_IRUSR) ? 'r' : '-',
			(ent_info->st_mode & S_IWUSR) ? 'w' : '-',
			user_execution_mode(ent_info->st_mode),
			(ent_info->st_mode & S_IRGRP) ? 'r' : '-',
			(ent_info->st_mode & S_IWGRP) ? 'w' : '-',
			group_execution_mode(ent_info->st_mode),
			(ent_info->st_mode & S_IROTH) ? 'r' : '-',
			(ent_info->st_mode & S_IWOTH) ? 'w' : '-',
			sticky_bit_execution_mode(ent_info->st_mode),
			ext_attr_mode(filename));
}
