/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   files.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 20:15:23 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:03:39 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int				file_ends_with_slash(char const *filename)
{
	return (filename[ft_strlen(filename) - 1] == '/');
}

int				is_file(t_exec_env *env, t_stat *info, char const *filename)
{
	return ((S_ISREG(info->st_mode)) ||
			(S_ISLNK(info->st_mode) &&
			 !file_ends_with_slash(filename) &&
			 env->options.l));
}

void			examine_file(t_exec_env *env, t_fs_entity *curr, t_width *w)
{
	if (env->options.l)
		verbose_info(env, curr, ft_strdup(curr->path), w);
	else
		ft_printf("%s\n", curr->path);
}

t_fs_entity		*copy_list(t_fs_entity *orig)
{
	t_fs_entity		*copy;

	copy = (t_fs_entity*)malloc(sizeof(t_fs_entity));
	copy->path = ft_strdup(orig->path);
	ft_memcpy(&copy->ent_info, &orig->ent_info, sizeof(t_stat));
	copy->next = NULL;
	return (copy);
}

t_fs_entity		*get_files_only(t_exec_env *env)
{
	t_fs_entity	*start;
	t_fs_entity	*curr;
	t_fs_entity	*iter;

	iter = env->entities;
	start = NULL;
	while (iter)
	{
		if (is_file(env, &iter->ent_info, iter->path))
		{
			if (start)
			{
				curr->next = copy_list(iter);
				curr = curr->next;
			}
			else
			{
				start = copy_list(iter);
				curr = start;
			}
		}
		iter = iter->next;
	}
	return (start);
}

void			set_file_widths(t_fs_entity *start, t_width *widths)
{
	if (start == NULL)
		return ;
	set_widths(start, widths);
	free_list(start);
}
