/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   entities.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 20:13:39 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/14 22:54:06 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		append_to_entities(t_exec_env *env,
				t_fs_entity **start, t_fs_entity *entity)
{
	t_fs_entity		*curr;
	t_fs_entity		*prev;

	if (!*start)
	{
		*start = entity;
		return ;
	}
	prev = NULL;
	curr = *start;
	while (curr)
	{
		if (env->options.r ? ft_strcmp(entity->path, curr->path) > 0 : 
					ft_strcmp(entity->path, curr->path) < 0)
		{
			if (prev)
				prev->next = entity;
			else
				*start = entity;
			entity->next = curr;
			return ;
		}
		prev = curr;
		curr = curr->next;
	}
	prev->next = entity;
}

void		delete_entity_where(t_fs_entity **start, char const *wrong_path)
{
	t_fs_entity		*curr;
	t_fs_entity		*prev;

	if (!*start)
		return ;
	curr = *start;
	if (!(ft_strcmp(curr->path, wrong_path)))
	{
		*start = (*start)->next;
		free(curr);
		return ;
	}
	prev = *start;
	curr = prev->next;
	while (curr)
	{
		if (!ft_strcmp(curr->path, wrong_path))
		{
			prev->next = curr->next;
			free(curr);
			return ;
		}
		curr = curr->next;
	}
}

void		add_entity(t_exec_env *env, t_fs_entity **start, char const *dir)
{
	t_fs_entity		*entity;

	entity = (t_fs_entity*)malloc(sizeof(t_fs_entity));
	if (!entity)
		perror(env->binary_name);
	entity->path = ft_strdup(dir);
	entity->next = NULL;
	append_to_entities(env, start, entity);
}

int			skip_options(int argc, char **argv)
{
	int		iter;

	iter = 1;
	while (iter < argc)
	{
		if (!check_option(argv[iter]))
			return (iter);
		iter++;
	}
	return (argc);
}

void		set_entities(t_exec_env *environment, int argc, char **argv)
{
	int		iter;

	iter = skip_options(argc, argv);
	if (iter == argc)
	{
		add_entity(environment, &environment->entities, ".");
		return ;
	}
	if (!ft_strcmp(argv[iter], "--"))
		iter++;
	while (iter < argc)
	{
		add_entity(environment, &environment->entities, argv[iter]);
		iter++;
	}
}
