/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_format.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 22:50:21 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:04:36 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char			*get_time_str(t_stat *ent_info)
{
	char	*time_str;

	time_str = ctime(&ent_info->st_mtimespec.tv_sec);
	return (time_str);
}

char const		*get_time(time_t file_time, char const *time_str)
{
	time_t	now;

	now = time(NULL);
	if ((now - file_time > 60 * 60 * 24 * 182) || (now - file_time < 3))
		return (time_str + 19);
	return (time_str + 11);
}

void			print_time(t_stat *ent_info)
{	
	char	*time_str;

	time_str = get_time_str(ent_info);
	ft_printf("%.3s %2.2s %.5s", time_str + 4, time_str + 8,
			get_time(ent_info->st_mtimespec.tv_sec, time_str));
}

void			print_device_number(t_stat *info, int width)
{
	int		minor_s;
	int		major_s;

	width = (width - 2) / 2;
	minor_s = minor(info->st_rdev);
	major_s = major(info->st_rdev);
	print_spaces(width - (major_s == 0 ? 1 : get_order_of_magn(major_s)));
	ft_printf("%i, ", major_s);
	print_spaces(width - (minor_s == 0 ? 1 : get_order_of_magn(minor_s)));
	ft_printf("%i ", minor_s);
}

void			verbose_info(t_exec_env *env, t_fs_entity *curr,
		char *full_path, t_width *widths)
{
	char	link_path[256];

	if (lstat(full_path, &curr->ent_info) == -1)
		return (stat_error(env, full_path));
	print_file_mode(&curr->ent_info, full_path);
	print_number_of_links(&curr->ent_info, widths->links_w);
	print_owner(&curr->ent_info, widths->user_w);
	print_group(&curr->ent_info, widths->group_w);
	if (S_ISCHR(curr->ent_info.st_mode) || S_ISBLK(curr->ent_info.st_mode))
		print_device_number(&curr->ent_info, widths->size_w);
	else
		print_byte_size(&curr->ent_info, widths->size_w);
	print_time(&curr->ent_info);
	ft_printf(" %s", curr->path);
	if (is_link(curr->ent_info.st_mode))
	{
		ft_bzero(link_path, 256);
		readlink(full_path, (char*)&link_path, 256);
		ft_printf(" -> %s ", link_path);
	}
	write(1, "\n", 1);
	free(full_path);
}
