/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 17:11:23 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:11:56 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>

void	nullify_env(t_exec_env *env)
{
	env->entities = NULL;
	env->options.c = 0;
	env->options.l = 0;
	env->options.a = 0;
	env->options.r = 0;
	env->options.t = 0;
	env->options.u = 0;
	env->options.f = 0;
	env->options.g = 0;
	env->options.d = 0;
	env->options.big_r = 0;
	env->options.big_g = 0;
}

int		initial_entities_cycle(t_exec_env *env,
		int (*cmp)(t_exec_env *, t_stat*, char const *),
			void (*examine)(t_exec_env*, t_fs_entity*, t_width *w))
{
	t_fs_entity		*curr;
	t_fs_entity		*prev;
	int				counter;
	t_width			widths;

	curr = env->entities;
	counter = 0;
	if (cmp == &is_file)
		set_file_widths(get_files_only(env), &widths);
	while (curr)
	{
		if (cmp(env, &curr->ent_info, curr->path))
		{
			examine(env, curr, &widths);
			if (cmp == &is_file)
			{
				prev = curr;
				curr = curr->next;
				delete_entity_where(&env->entities, prev->path);
			}
			else
				curr = curr->next;
			counter++;
		}
		else
			curr = curr->next;
	}
	return (counter);
}

void	directories_proc(t_exec_env *environment)
{
	initial_entities_cycle(environment, &is_dir, &examine_argv_dir);
}

void	files_first(t_exec_env *environment)
{
	int		res;

	set_stat_info(environment);
	environment->valid_entities_q = count_entities(environment->entities);
	res = initial_entities_cycle(environment,
			&is_file, &examine_file);
	if (res && res < environment->valid_entities_q)
		write(1, "\n", 1);
}

void	ls(t_exec_env *environment)
{
	files_first(environment);
	directories_proc(environment);
}

int		main(int argc, char **argv)
{
	t_exec_env		environment;

	nullify_env(&environment);
	environment.binary_name = argv[0];
	environment.argc = argc;
	environment.argv = argv;
	read_options(&environment, argc, argv);
	set_entities(&environment, argc, argv);
	ls(&environment);
}
