/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat_info.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/12 20:58:54 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:19:40 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char			filetype_char(int mode)
{
	if (S_ISREG(mode))
		return ('-');
	else if (S_ISDIR(mode))
		return ('d');
	else if (S_ISBLK(mode))
		return ('b');
	else if (S_ISCHR(mode))
		return ('c');
	else if (S_ISFIFO(mode))
		return ('p');
	else if (S_ISLNK(mode))
		return ('l');
	return ('?');
}

void			print_number_of_links(t_stat *ent_info, int width)
{
	print_spaces(width - (ent_info->st_nlink ?
				get_order_of_magn(ent_info->st_nlink) :
				1));
	ft_printf("%i ", ent_info->st_nlink);
}

void			print_owner(t_stat *ent_info, int width)
{
	struct passwd	*user_info;

	user_info = getpwuid(ent_info->st_uid);
	ft_printf("%s", user_info ? user_info->pw_name : "");
	print_spaces(width + 2 - (user_info ? ft_strlen(user_info->pw_name) : 0));
}

void			print_group(t_stat *ent_info, int width)
{
	struct group	*group_info;

	group_info = getgrgid(ent_info->st_gid);
	ft_printf("%s", group_info ? group_info->gr_name : "");
	print_spaces(width + 2 - (group_info ? ft_strlen(group_info->gr_name) : 0));
}

void			print_byte_size(t_stat *ent_info, int width)
{
	print_spaces(width - (ent_info->st_size ?
				get_order_of_magn(ent_info->st_size) :
				1));
	ft_printf("%i ", ent_info->st_size);
}
