/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directories.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 15:56:42 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:12:05 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int				is_dir(t_exec_env *env, t_stat *info, char const *filename)
{
	(void)filename;
	(void)env;
	return (S_ISDIR(info->st_mode));
}

t_fs_entity		*create_dir_entities(t_exec_env *env, DIR *dir)
{
	t_dirent	*dir_info;
	t_fs_entity	*start;

	start = NULL;
	while ((dir_info = readdir(dir)))
		add_entity(env, &start, dir_info->d_name);
	return (start);
}

char			*create_dir(char const *head, char const *dir)
{
	char	*temp;
	char	*res;

	temp = ft_strjoin(head, "/");
	res = ft_strjoin(temp, dir);
	free(temp);
	return (res);
}

int				path_is_not_only_dots(char const *dir_name)
{
	if (ft_strcmp(dir_name, ".") && ft_strcmp(dir_name, ".."))
		return (1);
	return (0);
}

size_t			get_long_total(t_exec_env *env, t_fs_entity *start, char const *head_path)
{
	char	*full_path;
	size_t	res;

	res = 0;
	while (start)
	{
		full_path = create_dir(head_path, start->path);
		if (lstat(full_path, &start->ent_info) != -1)
		{
			if (!is_hidden(start->path) || env->options.a)
				res += start->ent_info.st_blocks;
		}
		start = start->next;
		free(full_path);
	}
	return (res);
}

void			print_entities(t_exec_env *env, t_fs_entity *start,
					char const *head_path)
{
	t_fs_entity		*curr;
	t_width			widths;

	if (env->options.l)
	{
		ft_printf("total %i\n", get_long_total(env, start, head_path));
		set_widths(start, &widths);
	}
	curr = start;
	while (curr)
	{
		if (!is_hidden(curr->path) || (is_hidden(curr->path) && env->options.a))
		{
			env->options.l ? 
				verbose_info(env, curr,
						create_dir(head_path, curr->path), &widths) :
				ft_printf("%s\n", curr->path);
		}
		curr = curr->next;
	}
}

void			examine_dir_entities(t_exec_env *env, t_fs_entity *dir_entities,
		char const *head_dir_name)
{
	char	*curr_dir;

	while (dir_entities)
	{
		if (path_is_not_only_dots(dir_entities->path))
		{
			curr_dir = create_dir(head_dir_name, dir_entities->path);
			lstat(curr_dir, &dir_entities->ent_info);
			if (is_dir(env, &dir_entities->ent_info, NULL))
			{
				if (!is_hidden(dir_entities->path) || env->options.a)
				{
					ft_printf("\n%s:\n", curr_dir);
					examine_dir(env, curr_dir);
				}
			}
			free(curr_dir);
		}
		dir_entities = dir_entities->next;
	}
}

void			free_list(t_fs_entity *curr)
{
	if (!curr)
		return ;
	free((char*)curr->path);
	free_list(curr->next);
	free(curr);
}

void			examine_dir(t_exec_env *env, char const *curr)
{
	DIR			*dir;
	t_fs_entity	*dir_entities;

	if (!(dir = opendir(curr)))
		return ((void)opendir_error(dir, env, curr));
	dir_entities = create_dir_entities(env, dir);
	print_entities(env, dir_entities, curr);
	if (env->options.big_r)
		examine_dir_entities(env, dir_entities, curr);
	closedir(dir);
	free_list(dir_entities);
}

void			examine_argv_dir(t_exec_env *env, t_fs_entity *curr,
		t_width *w)
{
	DIR			*dir;

	(void)w;
	if (!(dir = opendir(curr->path)))
	{
		opendir_error(dir, env, curr->path);
		return ;
	}
	if (env->valid_entities_q > 1)
		ft_printf("%s:\n", curr->path);
	examine_dir(env, curr->path);
	if (curr->next)
		write(1, "\n", 1);
	closedir(dir);
}
