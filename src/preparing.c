/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   preparing.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/15 21:52:12 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 21:54:21 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		set_stat_info(t_exec_env *env)
{
	t_fs_entity		*curr;

	curr = env->entities;
	while (curr)
	{
		if (lstat(curr->path, &curr->ent_info) == -1)
			stat_error(env, curr->path);
		curr = curr->next;
	}
}
