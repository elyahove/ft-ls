/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options_validation.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 21:56:37 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 22:02:25 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		is_valid_option(char opt)
{
	if (opt == 'l' || opt == 'R' || opt == 'a' || opt == 'r' || opt == 't')
		return (1);
	return (0);
}

int		check_option(char *opt)
{
	if (opt[0] != '-' || !opt[1] || !ft_strcmp(opt, "--"))
		return (0);
	return (1);
}
