/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 19:17:17 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 23:05:02 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	assign_option(t_exec_env *environment, char opt)
{
	if (opt == 'l')
		environment->options.l = 1;
	else if (opt == 'R')
		environment->options.big_r = 1;
	else if (opt == 'a')
		environment->options.a = 1;
	else if (opt == 'r')
		environment->options.r = 1;
	else if (opt == 't')
		environment->options.t = 1;
}

void	parse_single_option(t_exec_env *environment, char opt)
{
	if (!is_valid_option(opt))
		illegal_option(environment, opt);
	assign_option(environment, opt);
}

int		parse_option(t_exec_env *environment, char *opt)
{
	if (!check_option(opt))
		return (0);
	while (*(++opt))
		parse_single_option(environment, *opt);
	return (1);
}

void	read_options(t_exec_env *environment, int argc, char **argv)
{
	int		iter;

	iter = 1;
	while (iter < argc)
	{
		if (!parse_option(environment, argv[iter]))
			break ;
		iter++;
	}
}
