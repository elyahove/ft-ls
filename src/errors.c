/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 21:47:55 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/13 22:16:10 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	illegal_option(t_exec_env *environment, char opt)
{
	ft_printf("%s: illegal option -- %c\nusage: %s [-lRart] [file ...]\n",
			environment->binary_name, opt, environment->binary_name);
	exit(1);
}

void	stat_error(t_exec_env *env, char const *curr_path)
{
	ft_printf("%s: %s: %s\n", env->binary_name,
			curr_path, strerror(errno));
	delete_entity_where(&env->entities, curr_path);
}

int		opendir_error(DIR *dir, t_exec_env *env, char const *path)
{
	(void)path;
	if (!dir)
	{
		perror(env->binary_name);
		return (1);
	}
	return (0);
}
