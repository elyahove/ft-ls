/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 16:53:51 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/15 22:11:35 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FT_LS_H
# define __FT_LS_H

# include <sys/types.h>
# include <sys/stat.h>
# include <sys/xattr.h>
# include <uuid/uuid.h>
# include <pwd.h>
# include <grp.h>
# include <dirent.h>
# include <errno.h>
# include <stdio.h>
# include <string.h>
# include <time.h>
# include "libft.h"
# include "ft_printf.h"

typedef struct stat		t_stat;
typedef struct dirent	t_dirent;

typedef struct	s_options
{
	unsigned short	c : 1;
	unsigned short	l : 1;
	unsigned short	a : 1;
	unsigned short	r : 1;
	unsigned short	t : 1;
	unsigned short	u : 1;
	unsigned short	f : 1;
	unsigned short	g : 1;
	unsigned short	d : 1;
	unsigned short	big_r : 1;
	unsigned short	big_g : 1;
}				t_options;

typedef struct	s_width
{
	int		links_w;
	int		user_w;
	int		group_w;
	int		size_w;
}				t_width;

typedef struct	s_fs_entity
{
	char const			*path;
	t_stat				ent_info;
	struct s_fs_entity	*next;
}				t_fs_entity;

typedef struct	s_exec_env
{
	char		*binary_name;
	int			argc;
	char		**argv;
	int			valid_entities_q;
	t_options	options;
	t_fs_entity	*entities;
}				t_exec_env;

void			ls(t_exec_env *env);
void			read_options(t_exec_env *env, int argc, char **argv);

/*
**	Options
*/

int				is_valid_option(char opt);
int				check_option(char *opt);

/*
**	Entities
*/

void			set_entities(t_exec_env *environment, int argc, char **argv);
void			add_entity(t_exec_env *env, t_fs_entity **st, char const *dir);
void			delete_entity_where(t_fs_entity **start, char const *wr_path);
int				count_entities(t_fs_entity *start);
void			free_list(t_fs_entity *curr);

/*
**	Stat
*/

char			filetype_char(int mode);
void			print_number_of_links(t_stat *ent_info, int width);
void			print_owner(t_stat *ent_info, int width);
void			print_group(t_stat *ent_info, int width);
void			print_byte_size(t_stat *ent_info, int width);
void			set_stat_info(t_exec_env *env);

/*
**	Long format
*/

void			verbose_info(t_exec_env *env, t_fs_entity *curr, char *full_p,
						t_width *widths);

/*
**	Permissions
*/

void			print_file_mode(t_stat *ent_info, char *filename);

/*
**	Hidden
*/

int				is_hidden(const char *path);

/*
**	Files
*/

int				is_file(t_exec_env *env, t_stat *info, char const *filename);
void			examine_file(t_exec_env *env, t_fs_entity *curr, t_width *w);
t_fs_entity		*get_files_only(t_exec_env *env);

/*
**	Directories
*/

int				is_dir(t_exec_env *env, t_stat *info, char const *filename);
void			examine_argv_dir(t_exec_env *env, t_fs_entity *curr, t_width *w);
void			examine_dir(t_exec_env *env, char const *curr);
void			examine_dir_entities(t_exec_env *env, t_fs_entity *dir_entities,
						char const *head_dir_name);
char			*create_dir(char const *head, const char *path);

/*
**	Links
*/

char			is_link(int mode);

/*
**	Widths
*/

void			set_widths(t_fs_entity *start, t_width *widths);
void			print_spaces(int width);
int				get_order_of_magn(long int num);
void			set_file_widths(t_fs_entity *start, t_width *widths);

/*
**	Errors
*/

void			illegal_option(t_exec_env *environment, char opt);
void			stat_error(t_exec_env *env, char const *c);
int				opendir_error(DIR *dir, t_exec_env *env, char const *path);

#endif
