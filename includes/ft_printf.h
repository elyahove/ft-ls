/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/20 20:22:11 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/06 19:16:01 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FT_PRINTF_H
# define __FT_PRINTF_H
# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>
# include <stdio.h>
# include <wctype.h>
# define FN_QNT 127
# define RED     "\x1b[31m"
# define GREEN   "\x1b[32m"
# define YELLOW  "\x1b[33m"
# define BLUE    "\x1b[34m"
# define MAGENTA "\x1b[35m"
# define CYAN    "\x1b[36m"
# define RESET   "\x1b[0m"
# define PREPARE res = (char*)malloc(5), ft_bzero(res, 5), *len = 0

int					ft_printf(const char *format, ...);

enum				e_flags
{
	HASH = 1,
	SPACE = 2,
	MINUS = 4,
	PLUS = 8,
	ZERO = 16
};

typedef struct		s_arg
{
	void			*arg;
	int				num;
	struct s_arg	*next;
}					t_arg;

typedef struct		s_format
{
	int				flag;
	char			spec;
	int				width;
	int				precision;
	int				len_type;
	int				strl;
	int				neg;
	char			*f_arg;
	t_arg			*arg;
}					t_format;

typedef struct		s_params
{
	int				printed;
	int				iter;
	char			*buff;
	int				buff_len;
	char			*(*func[FN_QNT])(struct s_format*, struct s_params**);
	struct s_arg	*curr;
	struct s_arg	*start;
	struct s_format	*frmt;
}					t_params;

typedef char		*(*t_conv_func)(struct s_format *, struct s_params **);

t_arg				*assign_vars(va_list args, int count, int num);
void				assign_flags(const char *format, t_format **current);
void				assign_min_max_width(const char *format, t_format **curr);
void				assign_type_len(const char *format, t_format **current);
void				assign_type_spec(const char *format, t_format **current);
void				assign_argument(const char *format, t_params **current);
void				assign_functions(t_conv_func *f);
void				print_hex_octal(t_format *f, t_params **p);
void				print_else(t_format *f, t_params **p);
void				print_number(t_format *f, t_params **p);
void				support_signs(t_format *f, t_params **p);
void				check_null(t_format *f, t_params **p);
void				get_formatted_arg(t_format *f, t_params **p);

int					is_specifier(char c);
int					is_valid_symbol(char c);
int					valid_zero_flag(char const *format, int i);
int					is_valid_type_len(char c);
int					has_function_in_arr(char c);
int					has_flag(t_format *f, int flag);
int					is_integer_spec(char c);
int					is_signed_spec(char c);
int					is_octal_hex(char c);
int					is_octal(char c);
int					count_specifiers(char const *format);
int					chars_to_next_format(char const *format);
int					to_next_integer(const char *format, int i);

void				analyze(const char *format, t_params **p);
void				print_analyze(t_format *f, t_params **p);
void				manage_color(char const *format, t_params **p);

t_format			*create_new_format(void);
void				nullify_format(t_format *res);

char				*ft_itoa(t_format *arg, t_params **p);
char				*ft_uitoa(t_format *arg, t_params **p);
char				*itoa_hex(t_format *arg, t_params **p);
char				*itoa_octal(t_format *arg, t_params **p);
char				*char_to_str(t_format *arg, t_params **p);
char				*get_w_string(t_format *arg, t_params **p);
char				*get_w_char(wchar_t hidden, int *len, int p);
char				*minus_sign_kost(char *str);
char				*ft_strdup(const char *s1);
char				*ft_strjoin(char const *s1, char const *s2);
int					ft_atoi(const char *str);
int					ft_strcmp(const char *s1, const char *s2);
size_t				ft_strlen(const char *str);

void				ft_putstr(char *str, int lim);
void				ft_bzero(void *s, size_t n);
void				itoa_norm(intmax_t num, size_t len, char **res);
void				assign_norm(t_params **current);
int					find_and_norm(char const *format, int *start, int *res);

intmax_t			return_type(void *arg, int flag);
uintmax_t			return_utype(void *arg, int flag);
#endif
