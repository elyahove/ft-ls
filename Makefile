# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: elyahove <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/06/06 17:00:17 by elyahove          #+#    #+#              #
#    Updated: 2017/06/15 21:55:15 by elyahove         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 			= ft_ls

FLAGS			= -Wall -Werror -Wextra -g

#=============PATHS=============#
SRC_PATH 		= ./src/
INCLUDES_PATH	= ./includes/
LIB_PATH		= ./libs/
LIBFT_PATH		= $(LIB_PATH)libft/
PRINTF_PATH		= $(LIBFT_PATH)ft_printf/
#===============================#

#========================FILES===========================#
SRC_FILES		= files.c options.c errors.c entities.c  \
				  ls.c options_validation.c directories.c\
				  hidden.c entities_count.c stat_info.c	 \
				  permissions.c long_format.c links.c	 \
				  width.c preparing.c

SRC 			= $(addprefix $(SRC_PATH),$(SRC_FILES))
OBJ				= $(SRC:.c=.o)
PRINTF			= $(PRINTF_PATH)libftprintf.a
LIBFT			= $(LIBFT_PATH)libft.a
#========================================================#

.PHONY: lib
.PHONY: lib_clean
.PHONY: lib_fclean


%.o:%.c
	gcc $(FLAGS) -c -o $@ $< -I$(INCLUDES_PATH)


$(NAME): lib $(OBJ)
	gcc $(FLAGS) $(OBJ) $(LIBFT) $(PRINTF) -o $(NAME) -I$(INCLUDES_PATH)

all: $(NAME)

clean: lib_clean
	rm -f $(OBJ)

fclean: lib_fclean clean
	rm -f $(NAME)

re: fclean all

lib:
	make -C $(LIBFT_PATH)

lib_clean:
	make clean -C $(LIBFT_PATH)

lib_fclean:
	make fclean -C $(LIBFT_PATH)
